package com.example.book.repository.remote

import android.util.Log
import com.example.book.R
import com.example.book.util.datatype.Book
import com.example.book.util.datatype.CartItem
import com.example.book.util.datatype.Category
import com.example.book.util.datatype.User

object api {
    private var user: User = User("admin@mail.com", "admin","admin")

    private val books = listOf<Book>(
        Book(1, "مشکل ستاره های ما", R.drawable.the_falt_in_our_stars, "عاشقانه", 30000, "John Green", listOf(R.drawable.the_falt_in_our_stars, R.drawable.the_falt_in_our_stars, R.drawable.the_falt_in_our_stars, R.drawable.the_falt_in_our_stars, R.drawable.the_falt_in_our_stars, )),
        Book(2, "مکزیکی وحشی", R.drawable.mexican_gothic, "رمان", 10000, "Silvia Moreno-Garcia", listOf(R.drawable.mexican_gothic, R.drawable.mexican_gothic, R.drawable.mexican_gothic, R.drawable.mexican_gothic, R.drawable.mexican_gothic, )),
        Book(3, "سربازگیری", R.drawable.recruitment, "رمان", 25000, "K. a. riley", listOf(R.drawable.recruitment, R.drawable.recruitment, R.drawable.recruitment, R.drawable.recruitment, R.drawable.recruitment, )),
        Book(4, "نیمه ناپدید شده", R.drawable.vanishing_half, "رمان", 27000, "Brit Bennett", listOf(R.drawable.vanishing_half, R.drawable.vanishing_half, R.drawable.vanishing_half, R.drawable.vanishing_half, R.drawable.vanishing_half, )),
        Book(5, "میخواهم", R.drawable.want, "عاشقانه", 21000, "Lynn Steger Strong", listOf(R.drawable.want, R.drawable.want, R.drawable.want, R.drawable.want, R.drawable.want, )),
        Book(6, "عشق و بستنی", R.drawable.love_and_gelato, "عاشقانه", 15000, "Jenna Evan Welch", listOf(R.drawable.love_and_gelato, R.drawable.love_and_gelato, R.drawable.love_and_gelato, R.drawable.love_and_gelato, R.drawable.love_and_gelato, )),
        Book(7, "ملکه دل ها", R.drawable.the_queen_of_hearts, "عاشقانه", 17000, "Kimmery Martin", listOf(R.drawable.the_queen_of_hearts, R.drawable.the_queen_of_hearts, R.drawable.the_queen_of_hearts, R.drawable.the_queen_of_hearts, R.drawable.the_queen_of_hearts, )),
        Book(8, "آواتار - سایه کیوشی", R.drawable.avatar_the_shadow_of_kioshi, "کامیک", 31000, "F. C. YEE", listOf(R.drawable.avatar_the_shadow_of_kioshi, R.drawable.avatar_the_shadow_of_kioshi, R.drawable.avatar_the_shadow_of_kioshi, R.drawable.avatar_the_shadow_of_kioshi, R.drawable.avatar_the_shadow_of_kioshi, )),
        Book(9, "امید", R.drawable.faith, "کامیک", 26000, "Julie Murphy", listOf(R.drawable.faith, R.drawable.faith, R.drawable.faith, R.drawable.faith, R.drawable.faith, )),
        Book(10, "افسانه شمالی", R.drawable.norse_mythology, "کامیک", 41000, "Neil Gaiman", listOf(R.drawable.norse_mythology, R.drawable.norse_mythology, R.drawable.norse_mythology, R.drawable.norse_mythology, R.drawable.norse_mythology, )),
        Book(11, "هنر ظریف اهمیت ندادن", R.drawable.the_subtle_art_of_not_giving_a_fuck, "فلسفه", 27000, "Mark Manson", listOf(R.drawable.the_subtle_art_of_not_giving_a_fuck, R.drawable.the_subtle_art_of_not_giving_a_fuck, R.drawable.the_subtle_art_of_not_giving_a_fuck, R.drawable.the_subtle_art_of_not_giving_a_fuck, R.drawable.the_subtle_art_of_not_giving_a_fuck, )),
        Book(12, "کمیاگر", R.drawable.the_alchemist, "فلسفه", 15000, "Panlo Coelho", listOf(R.drawable.the_alchemist, R.drawable.the_alchemist, R.drawable.the_alchemist, R.drawable.the_alchemist, R.drawable.the_alchemist, )),
    )

    private val cart = arrayListOf<CartItem>(
   )


    fun forgetPassword(email: String) {
        Log.i("Remote", "Forget password: $email")
    }

    fun signUp(name: String, email: String, pass: String) {
        this.user = User(
            name= name,
            email = email,
            pass = pass
        )
        Log.i("Remote", "Sign up: $user")
    }

    fun signIn(email: String, pass: String): Boolean =
        ((user.email == email) and (user.pass == pass)).also {
            Log.i("Remote", "sign in: $it")
        }

    fun getBooksList(): List<Book> = books.also {
        Log.i("Remote", "books: ${it.size}")
    }

    fun getBooksList(category: String): List<Book> = books/*.filter { book ->
        book.category == category
    }*/.also {
        Log.i("Remote", "books-category: ${it.size}")
    }

    fun getUser(): User = this.user. also {
        Log.i("Remote", "get user: $it")
    }

    fun getCategories(): List<Category> = listOf(
        Category("عاشقانه", books),
        Category("رمان", books),
        Category("کامیک", books),
        Category("فلسفه", books),
    ).also {
        Log.i("Remote", "categories: ${it.size}")
    }

    fun getCart(): List<CartItem>  = cart.also {
        Log.i("Remote", "cart: ${it.size}")
    }

    fun getBook(bookId: Int): Book = this.books.first { book ->
        book.id == bookId
    }.also {
        Log.i("Remote", "book: $it")
    }

    fun getTerm(): String = """
        قوانین
        قوانین
        قوانین
        قوانین
        قوانین
        قوانین
        قوانین
        قوانین
        قوانین
        قوانین
        قوانین
    """.also {
        Log.i("Remote", "term: term")
    }

    fun updateUser(name: String, email: String, address: String, postcode: String): Boolean {
        this.user.name = name
        this.user.email = email
        this.user.address = address
        this.user.postCode = postcode
        Log.i("Remote", "update user: ${this.user}")
        return true
    }

    fun addToCart(bookId: Int): Boolean = try {
            val book = this.books.first{ book -> book.id == bookId }
            try {
                val index = this.cart.indexOfFirst { item -> item.book.id == book.id }
                this.cart[index].count ++
            } catch (e: java.lang.Exception) {
                this.cart.add(CartItem(book))
            }
            Log.i("Remote", "add: ${cart.size}")
            true
        } catch (e: Exception) {
            false
        }

    fun cleanCart() {
        this.cart.clear()
    }

    fun post(): Int {
        return  250000
    }

    fun cartIsEmpty(): Boolean = this.cart.isEmpty()

}