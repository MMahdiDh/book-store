package com.example.book.repository

import android.util.Log
import com.example.book.repository.local.dao
import com.example.book.repository.remote.api
import com.example.book.util.datatype.Book
import com.example.book.util.datatype.CartItem
import com.example.book.util.datatype.Category
import com.example.book.util.datatype.User

object Repository {
    suspend fun forgetPassword(email: String) {
        api.forgetPassword(email)
    }

    suspend fun signUp(name: String, email: String, pass: String) {
        api.signUp(name, email, pass)
    }

    fun signIn(email: String, pass: String, stay: Boolean): Boolean {
        return api.signIn(email, pass) .also {
           if (it) {
               dao.stay(stay)
           }
        }
    }

    fun autoSignIn(): Boolean = if (dao.isStainedSignIn()) {
        val user = dao.getUser()
        signIn(user.email, user.pass, false)
    } else {
        false
    }

    fun userInfo(): User {
        return api.getUser()
    }

    suspend fun getBooksList(): List<Book> {
        return api.getBooksList()
    }

    suspend fun getBooksList(category: String): List<Book> {
        return api.getBooksList(category)
    }

    suspend fun getCategories(): List<Category> {
        return api.getCategories()
    }

    fun getBook(bookId: Int): Book {
        return api.getBook(bookId)
    }

    suspend fun getCart(): List<CartItem> {
        return api.getCart()
    }

    fun getTerm(): String {
         return api.getTerm()
    }

    suspend fun updateUser(name: String, email: String, address: String, postcode: String) {
        if (api.updateUser(name, email, address, postcode)) {
            dao.getUser()
        }
    }

    suspend fun addToCart(bookId: Int): Boolean {
        return api.addToCart(bookId)
    }

    fun cleanCart() {
        api.cleanCart()
    }

    suspend fun post() = api.post()
    fun cartIsEmpty(): Boolean = api.cartIsEmpty()
    fun logOut() {
        dao.stay(false)
    }

}