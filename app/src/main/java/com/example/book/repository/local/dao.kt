package com.example.book.repository.local

import com.example.book.util.datatype.User

object dao {
    private var stay: Boolean = true

    private var user: User = User("admin@mail.com", "admin","admin")

    fun isStainedSignIn(): Boolean = stay

    fun getUser(): User = user

    fun stay(stay: Boolean) {
        this.stay = stay
    }
}