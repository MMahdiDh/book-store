package com.example.book.util.datatype

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

data class CartItem(
    val book: Book,
    var count: Int = 1
)