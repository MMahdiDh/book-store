package com.example.book.util.datatype

data class User(
    var email: String,
    var pass: String,
    var name: String = "",
    var address: String = "",
    var postCode: String = ""
)
