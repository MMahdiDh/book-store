package com.example.book.util.datatype

data class Book (
    val id: Int,
    val name: String,
    val imgUrl: Int,
    val category: String,
    val cost: Int,
    val writer: String,
    val screens: List<Int>,
    val off: Double = 0.0,
)