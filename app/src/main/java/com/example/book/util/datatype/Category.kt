package com.example.book.util.datatype

data class Category(
    val name: String,
    val books: List<Book>
)
