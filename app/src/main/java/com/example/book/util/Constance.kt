package com.example.book.util

const val SIGN_UP_LINK = "اکانت نداری؟ از اینجا ثبت نام کن"
const val EMAIL_ALERT = " ایمیل خودرا وارد نکردید"
const val APP_NAME = "کتابسرا"
const val FORGET_LINK = "رمز خود را فراموش کردی؟"
const val INTRO = "کتاب دنیای از ناشناخته ها"
const val COST_SIGN = "﷼"
const val BUY = "خرید"
const val NUMBER_TITLE = "تعداد:"
const val IS_STAY_LOG_IN = "به خاطر سپردن"
const val TERM_BTN = "متوجه شدم"
const val TERM_CHEK_BOX = "قوانینو ومقرارت را می پذیرم"
const val SIGN_UP_DEGREE = "قوانین را نپذیرفته اید"
const val SIGN_UP_NOTـMACH_PASS = "پسورد وادرد شده هماهنگ نیست"
const val TERM_TITTLE = "قوانین و مقرارات"

