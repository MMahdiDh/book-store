package com.example.book.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.book.databinding.ItemBookRecyclerBinding
import com.example.book.util.datatype.Book

class BooksAdapterRecycler(
    private val func: (Int)->Unit
): ListAdapter<Book,BooksAdapterRecycler.VH>(object : DiffUtil.ItemCallback<Book>() {
    override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean =
        newItem.id == oldItem.id

    override fun areContentsTheSame(oldItem: Book, newItem: Book): Boolean =
        newItem == oldItem

}){
    class VH(private val binding: ItemBookRecyclerBinding): RecyclerView.ViewHolder(binding.root) {
        fun set(dataset: Book, func: (Int)->Unit) {
            binding.apply {
                txtTitle.text = dataset.name

                imgBookCover.apply {
                    setImageResource(dataset.imgUrl)
                    contentDescription = "Book: ${dataset.name}"
                    setOnClickListener { func(dataset.id) }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(
        ItemBookRecyclerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.set(getItem(position), func)
    }
}