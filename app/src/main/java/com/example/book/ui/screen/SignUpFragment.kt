package com.example.book.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.book.databinding.FragmentSignUpBinding
import com.example.book.ui.viewmodel.SignUpViewModel
import com.example.book.util.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class SignUpFragment : Fragment() {

    private lateinit var binding: FragmentSignUpBinding
    private lateinit var viewModel: SignUpViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSignUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SignUpViewModel::class.java)

        binding.apply {
            txtTerm.text = TERM_TITTLE
            txtTerm.setOnClickListener {
                MaterialAlertDialogBuilder(this@SignUpFragment.requireContext()).apply {
                    setTitle(TERM_TITTLE)
                    setMessage(viewModel.getTerm())
                    setPositiveButton(TERM_BTN) { dialog, which ->
                        // Respond to positive button press
                    }
                }.show()
            }
            chkbxTerm.text = TERM_CHEK_BOX
            btnSend.setOnClickListener {
                val name = dtxtName.text.toString()
                val email = dtxtEmail.text.toString()
                val pass = dtxtPass.text.toString()
                val passC = dtxtConfirmPass.text.toString()
                val agree = chkbxTerm.isChecked

                if (agree) {
                    if (viewModel.isEmailValid(email)) {
                        if (pass == passC) {
                            viewModel.signUp(name, email, pass)
                            findNavController()
                                .navigate(SignUpFragmentDirections.actionSignUpToSignIn())
                        } else {
                            Toast.makeText(requireContext(), SIGN_UP_NOTـMACH_PASS, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(requireContext(), "ایمیل نامعتیر است", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(requireContext(), SIGN_UP_DEGREE, Toast.LENGTH_SHORT).show()
                }
            }
            txtAppName.text = APP_NAME

        }
    }

}