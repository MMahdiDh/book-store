package com.example.book.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

import com.example.book.databinding.ItemBookScreenBinding

class ScreenAdapter: ListAdapter<Int,ScreenAdapter.VH>(object : DiffUtil.ItemCallback<Int>(){
    override fun areItemsTheSame(oldItem: Int, newItem: Int): Boolean = newItem == oldItem

    override fun areContentsTheSame(oldItem: Int, newItem: Int): Boolean = newItem == oldItem

}) {
    class VH(private val binding: ItemBookScreenBinding): RecyclerView.ViewHolder(binding.root) {
        fun set(dataset: Int){
            binding.apply {
                 imgBookCover.setImageResource(dataset)
                imgBookCover.contentDescription = "Book's Screens"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(
        ItemBookScreenBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.set(getItem(position))
    }
}