package com.example.book.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.book.databinding.ItemBookBuyBinding
import com.example.book.util.COST_SIGN
import com.example.book.util.NUMBER_TITLE
import com.example.book.util.datatype.CartItem

class CartAdapter(
    private val func: (count: Int, price: Int, off: Double)->Unit
): ListAdapter<CartItem, CartAdapter.VH>(object : DiffUtil.ItemCallback<CartItem>(){
    override fun areItemsTheSame(oldItem: CartItem, newItem: CartItem): Boolean = newItem.book.id == oldItem.book.id

    override fun areContentsTheSame(oldItem: CartItem, newItem: CartItem): Boolean = newItem == oldItem

}) {
    class VH(private val binding: ItemBookBuyBinding): RecyclerView.ViewHolder(binding.root) {

        fun set(
            dataset: CartItem,
            update: (count: Int, price:Int, off: Double)->Unit
        ) {
            binding.apply {
                update(dataset.count, dataset.book.cost, dataset.book.off)
                imgBook.setImageResource(dataset.book.imgUrl)
                imgBook.contentDescription = "image: ${dataset.book.name}"
                txtBook.text = dataset.book.name
                txtWritter.text = dataset.book.writer
                txtCost.text = "${COST_SIGN}${dataset.book.cost}"
                txtNumberTitle.text = NUMBER_TITLE
                binding.txtNumber.text = dataset.count.toString()
                imgIncrease.setOnClickListener {
                    dataset.count ++
                    binding.txtNumber.text = dataset.count.toString()
                    update(1, dataset.book.cost, dataset.book.off)
                }
                imgDecrease.setOnClickListener {
                    if (dataset.count > 0) {
                        dataset.count --
                        binding.txtNumber.text = dataset.count.toString()
                        update(-1, dataset.book.cost, dataset.book.off)
                    }
                }

            }
        }

        fun getCount(count: Int){

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(
        ItemBookBuyBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.set(getItem(position), func)
    }
}