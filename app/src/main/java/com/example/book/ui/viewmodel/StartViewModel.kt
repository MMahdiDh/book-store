package com.example.book.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.book.repository.Repository

class StartViewModel : ViewModel() {
    fun autoSignIn(): Boolean = Repository.autoSignIn()
}