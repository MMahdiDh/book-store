package com.example.book.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.book.databinding.FragmentHomeBinding
import com.example.book.ui.adapter.BooksAdapterHome
import com.example.book.ui.viewmodel.HomeViewModel
import com.example.book.util.APP_NAME

class HomeFragment : Fragment() {

    private lateinit var binding:FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[HomeViewModel::class.java]

        binding.apply {
            rclBooks.apply {
                layoutManager = GridLayoutManager(activity,2)
                adapter = BooksAdapterHome(){ bookId ->
                    findNavController()
                        .navigate(HomeFragmentDirections.actionHomeToDetailFragment(bookId))
                }.also { adapter ->
                    viewModel.books.observe(viewLifecycleOwner) { dataset ->
                        adapter.submitList(dataset)
                    }
                }
            }.also { viewModel.getBooksList() }

            txtAppName.text = APP_NAME
        }
    }

}