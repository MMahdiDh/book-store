package com.example.book.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.book.repository.Repository
import kotlinx.coroutines.launch

class SignUpViewModel : ViewModel() {
    fun signUp(name: String, email: String, pass: String) {
        viewModelScope.launch {
            Repository.signUp(name, email, pass)
        }
    }

    fun getTerm(): String = Repository.getTerm()

    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

}