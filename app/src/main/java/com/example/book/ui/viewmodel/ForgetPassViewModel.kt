package com.example.book.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.book.repository.Repository
import kotlinx.coroutines.launch

class ForgetPassViewModel : ViewModel() {
    fun forgetPassword(email: String) {
        viewModelScope.launch {
            Repository.forgetPassword(email)
        }
    }
}