package com.example.book.ui.viewmodel


import androidx.lifecycle.*
import com.example.book.repository.Repository
import com.example.book.util.datatype.CartItem
import kotlinx.coroutines.launch

class CartViewModel : ViewModel() {

    private var _cart = MutableLiveData<List<CartItem>>()
    val cart: LiveData<List<CartItem>>
        get() = _cart

    private val _cost = MutableLiveData(0)
    val cost: LiveData<Int>
        get() = _cost

    private val _off = MutableLiveData(0)
    val off: LiveData<Int>
        get() = _off

    private val _pay = MutableLiveData(0)
    val pay: LiveData<Int>
        get() = _pay

    private val _send = MutableLiveData(0)
    val send: LiveData<Int>
        get() = _send
    private var _sendOld = 0

     fun update(count: Int, cost: Int, off: Double) {
         val c = cost * count
         val o = (cost * off) * count
         this._cost.value = this._cost.value!! + c
         this._off.value = this._off.value!! + o.toInt()
         this._pay.value = this._pay.value!! + (c - o).toInt()
     }


    fun getBooks() {
        viewModelScope.launch {
            _cart.value = Repository.getCart()
        }
    }

    fun post() {
        viewModelScope.launch {
            _sendOld = _send.value!!
            _send.value = Repository.post()
            _pay.value = _pay.value!! - _sendOld
            _pay.value = _pay.value!! + _send.value!!
        }
    }

    fun cartIsEmpty() {
        if (Repository.cartIsEmpty()) {
            _cost.value = 0
            _off. value = 0
            _pay. value = 0
            _send.value = 0
            _sendOld = 0
        }
    }

    fun isEmpty(): Boolean {
        return if (_sendOld == 0) true else false
    }
}