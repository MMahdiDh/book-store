//package com.example.book.ui.adapter
//
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.recyclerview.widget.DiffUtil
//import androidx.recyclerview.widget.ListAdapter
//import androidx.recyclerview.widget.RecyclerView
//import com.example.book.R.drawable.notfound
//import com.example.book.databinding.ItemBookBinding
//import com.example.book.util.datatype.Book
//import com.example.book.util.imgMap
//
//class BooksAdapter(
//    private val clickable: Boolean,
//    private val showTitle: Boolean,
//    private val func: (Int)->Unit
//): ListAdapter<Book, BooksAdapter.VH>(object : DiffUtil.ItemCallback<Book>() {
//        override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean =
//            newItem.id == oldItem.id
//
//        override fun areContentsTheSame(oldItem: Book, newItem: Book): Boolean =
//            newItem == oldItem
//
//    }) {
//    class VH(private val binding: ItemBookBinding): RecyclerView.ViewHolder(binding.root) {
//        fun set(dataset: Book, clickable: Boolean, showTitle: Boolean, func: (Int)->Unit) {
//            binding.apply {
//                txtTitle.apply {
//                    text = dataset.name
//                    visibility = if (showTitle) View.VISIBLE else View.GONE
//                }
//
//                imgBookCover.apply {
//                    setImageResource(imgMap[dataset.imgUrl]?: notfound)
//                    contentDescription = "Book: ${dataset.name}"
//                    if (clickable) setOnClickListener { func(dataset.id) }
//                }
//            }
//        }
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH (
//        ItemBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)
//    )
//
//    override fun onBindViewHolder(holder: VH, position: Int) {
//        holder.set(getItem(position), clickable, showTitle, func)
//    }
//}
