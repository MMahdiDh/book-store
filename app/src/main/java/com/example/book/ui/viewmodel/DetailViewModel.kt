package com.example.book.ui.viewmodel

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.book.repository.Repository
import com.example.book.util.datatype.Book
import com.example.book.util.datatype.Category
import kotlinx.coroutines.launch

class DetailViewModel : ViewModel() {

    lateinit var book: Book
    val recommendation = "پرفروش"

        private var _category = MutableLiveData<List<Category>>()
        val category: LiveData<List<Category>>
        get() = _category



     fun getBook(bookId: Int) {
        book = Repository.getBook(bookId)
    }

    fun getCategories() {
        viewModelScope.launch {
            _category.value = listOf(
                Category(recommendation, Repository.getBooksList(recommendation)),
                Category(book.category, Repository.getBooksList(book.category,)),
            )
        }
    }

    fun addToCart(func: ()->Unit) {
        viewModelScope.launch {
            if (Repository.addToCart(book.id)) func()
        }
    }

}