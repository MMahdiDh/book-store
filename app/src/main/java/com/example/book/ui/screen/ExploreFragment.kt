package com.example.book.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.core.view.marginLeft
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.book.databinding.FragmentExploreBinding
import com.example.book.ui.adapter.BooksAdapterRecycler
import com.example.book.ui.adapter.RecyclerAdapter
import com.example.book.ui.viewmodel.ExploreViewModel
import java.lang.NullPointerException

class ExploreFragment : Fragment() {

    private lateinit var binding: FragmentExploreBinding
    private lateinit var viewModel: ExploreViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentExploreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[ExploreViewModel::class.java]

        binding.apply {
            rclBooks.apply {
                try {
                    val arg:String = requireArguments().get("category")!!.toString()
                    layoutManager = GridLayoutManager(activity,3)
                    adapter = BooksAdapterRecycler() { bookId ->
                        findNavController()
                            .navigate(ExploreFragmentDirections.actionExploreToDetailFragment(bookId))
                    }.also { adapter ->
                        viewModel.books.observe(viewLifecycleOwner) { dataset ->
                            adapter.submitList(dataset)
                        }
                    }
                    viewModel.getBooksList(arg)
                } catch (e: NullPointerException) {
                    layoutManager = LinearLayoutManager(activity)
                    adapter = RecyclerAdapter(activity) { flag, category, bookId ->
                        findNavController().navigate(when (flag) {
                            "C" -> ExploreFragmentDirections.actionExploreSelf(category)
                            else -> ExploreFragmentDirections.actionExploreToDetailFragment(bookId)
                        })
                    }.also {  adapter ->
                        viewModel.categories.observe(viewLifecycleOwner) { dataset ->
                            adapter.submitList(dataset)
                        }
                    }
                    viewModel.getCategories()
                }
            }
        }
    }

}