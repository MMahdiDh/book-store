package com.example.book.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.book.repository.Repository
import com.example.book.util.datatype.User
import kotlinx.coroutines.launch

class PersonViewModel : ViewModel() {
    var user: User = Repository.userInfo()
    fun updateUser(name: String, email: String, address: String, postcode: String) {
        viewModelScope.launch {
            Repository.updateUser(name, email, address, postcode)
        }
    }

    fun logOut() {
        Repository.logOut()
    }

    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}