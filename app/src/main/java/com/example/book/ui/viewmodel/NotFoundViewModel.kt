package com.example.book.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.book.repository.Repository

class NotFoundViewModel : ViewModel(){
    fun cleanCart() {
        Repository.cleanCart()
    }
}