package com.example.book.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.book.R
import com.example.book.databinding.FragmentPersonBinding
import com.example.book.ui.viewmodel.PersonViewModel

class PersonFragment : Fragment() {

    private lateinit var binding: FragmentPersonBinding
    private lateinit var viewModel: PersonViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPersonBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[PersonViewModel::class.java]

        set(false)
        binding.apply {
            btnRecord.text = "ثبت"
            btnRecord.setOnClickListener {
                var name = dtxtName.text.toString()
                var email = dtxtEmail.text.toString()
                val address = dtxtAddress.text.toString()
                val postcode = dtxtPostcode.text.toString()
                if (viewModel.isEmailValid(email)) {
                    name = if (name.isNotBlank()) name else viewModel.user.name
                    email = if (email.isNotBlank()) email else viewModel.user.email
                    if (name.isNotBlank() and email.isNotBlank()) {
                        viewModel.updateUser(name, email, address, postcode)
                        set(false)
                    }
                    hintUpdate()
                } else {
                    Toast.makeText(requireContext(), "ایمیل نامعتیر است", Toast.LENGTH_SHORT).show()
                }
            }

            txtAddress.text = "آدرس:"
            txtPostcode.text = "کدپستی:"
            txtEmail.text = "ایمیل:"
            txtName.text = "نام:"
            hintUpdate()
            imgEdit.setOnClickListener { set(true) }
            imgLogOut.setOnClickListener {
                viewModel.logOut()
                findNavController().navigate(
                    PersonFragmentDirections.actionPersonToStart()
                )
            }
        }
    }

    private fun set(status: Boolean) { binding.apply {
        dtxtName.isEnabled = status
        dtxtEmail.isEnabled = status
        dtxtAddress.isEnabled = status
        dtxtPostcode.isEnabled = status
        btnRecord.visibility = if (status) View.VISIBLE else View.GONE
    } }

    private fun hintUpdate() { binding.apply {
        dtxtAddress.hint = viewModel.user.address
        dtxtPostcode.hint = viewModel.user.postCode
        dtxtEmail.hint = viewModel.user.email
        dtxtName.hint = viewModel.user.name

        dtxtAddress.text.clear()
        dtxtPostcode.text.clear()
        dtxtEmail.text.clear()
        dtxtName.text.clear()
    }}

}