package com.example.book.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.book.repository.Repository
import com.example.book.util.datatype.Book
import com.example.book.util.datatype.Category
import kotlinx.coroutines.launch

class ExploreViewModel : ViewModel() {
    private var _categories = MutableLiveData<List<Category>>()
    val categories: LiveData<List<Category>>
        get() = _categories

    private var _books = MutableLiveData<List<Book>>()
    val books: LiveData<List<Book>>
        get() = _books

    fun getCategories() { viewModelScope.launch {
            _categories.value = Repository.getCategories()
    } }

    fun getBooksList(category: String) { viewModelScope.launch {
            _books.value = Repository.getBooksList(category)
    } }
}