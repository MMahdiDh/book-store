package com.example.book.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.book.R
import com.example.book.databinding.FragmentForgetPassBinding
import com.example.book.ui.viewmodel.ForgetPassViewModel
import com.example.book.util.EMAIL_ALERT
import com.example.book.util.SIGN_UP_LINK

class ForgetPassFragment : Fragment() {

    private lateinit var binding: FragmentForgetPassBinding
    private lateinit var viewModel: ForgetPassViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentForgetPassBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[ForgetPassViewModel::class.java]

        binding.apply {
            txtSinUp.text = SIGN_UP_LINK
            txtSinUp.setOnClickListener {
                findNavController()
                    .navigate(ForgetPassFragmentDirections.actionForgetPassToSignUp())
            }
            btnSend.setOnClickListener {
                val email = dtxtEmail.text.toString()
                if (email.isBlank()) {
//                    dtxtEmail.isErrorEnabled = true
                } else {
                    viewModel.forgetPassword(email)
                    Toast.makeText(context,EMAIL_ALERT,Toast.LENGTH_SHORT).show()
                    findNavController()
                        .navigate(ForgetPassFragmentDirections.actionForgetPassToSignIn())
                }
            }
        }
    }

}