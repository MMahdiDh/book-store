package com.example.book.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.book.databinding.FragmentStartBinding
import com.example.book.ui.viewmodel.StartViewModel
import com.example.book.util.APP_NAME
import com.example.book.util.INTRO

class StartFragment : Fragment() {

    private lateinit var binding: FragmentStartBinding
    private lateinit var viewModel: StartViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[StartViewModel::class.java]
        if (viewModel.autoSignIn()) {
            findNavController()
                .navigate(StartFragmentDirections.actionStartToHome())
        }

        binding.apply {
            btnStart.setOnClickListener {
                findNavController()
                    .navigate(StartFragmentDirections.actionStartToSignIn())
            }
            txtIntro.text = INTRO
            txtAppName.text = APP_NAME
        }
    }

}