package com.example.book.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.book.databinding.FragmentDetailBinding
import com.example.book.ui.adapter.RecyclerAdapter
import com.example.book.ui.adapter.ScreenAdapter
import com.example.book.ui.viewmodel.DetailViewModel
import com.example.book.util.BUY
import com.example.book.util.COST_SIGN

class DetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailBinding
    private lateinit var viewModel: DetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[DetailViewModel::class.java]

        val arg = requireArguments().get("bookId") as Int
        if (arg == -1) { TODO() } else { viewModel.getBook(arg) }

        binding.apply {
            rclCategories.apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = RecyclerAdapter(activity) { flag, category, bookId ->
                    findNavController().navigate(
                        when (flag) {
                            "C" -> DetailFragmentDirections.actionDetailFragmentToExplore(category)
                            else -> DetailFragmentDirections.actionDetailFragmentSelf(bookId)
                        }
                    )
                }.also { adapter ->
                    viewModel.category.observe(viewLifecycleOwner) { dataset ->
                        adapter.submitList(dataset)
                    }
                }
            }.also { viewModel.getCategories() }

            // Book Detail
            viewModel.book.apply {
                txtCost.text = cost.toString()
                txtCostSign.text = COST_SIGN

                txtCategory.text = category
                crdCategory.setOnClickListener {
                    findNavController().navigate(
                        DetailFragmentDirections.actionDetailFragmentToExplore(category)
                    )
                }

                txtBookWriter.text = writer
                txtBookTitle.text = name

                txtBuy.text = BUY
                crdBuy.setOnClickListener { viewModel.addToCart(){
                    Toast.makeText(requireContext(), "به سبد خرید اضافه شد.", Toast.LENGTH_LONG).show()
                } }

                rclBook.apply {
                    layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
                    adapter = ScreenAdapter().also { adapter ->
                        adapter.submitList(listOf(imgUrl) + screens)
                    }
                }
            }
        }
    }
}