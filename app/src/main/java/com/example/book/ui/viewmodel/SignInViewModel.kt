package com.example.book.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.book.repository.Repository

class SignInViewModel : ViewModel() {
    fun signIn(email: String, pass: String, stay: Boolean): Boolean =
        Repository.signIn(email, pass, stay)
}