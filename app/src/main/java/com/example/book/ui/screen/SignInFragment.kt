package com.example.book.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.book.R
import com.example.book.databinding.FragmentSignInBinding
import com.example.book.ui.viewmodel.SignInViewModel
import com.example.book.util.APP_NAME
import com.example.book.util.FORGET_LINK
import com.example.book.util.IS_STAY_LOG_IN
import com.example.book.util.SIGN_UP_LINK

class SignInFragment : Fragment() {

    private lateinit var binding: FragmentSignInBinding
    private lateinit var viewModel: SignInViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSignInBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[SignInViewModel::class.java]

        binding.apply {
            txtSignUp.text = SIGN_UP_LINK
            txtSignUp.setOnClickListener {
                findNavController()
                    .navigate(SignInFragmentDirections.actionSignInToSignUp())
            }

            txtForget.text = FORGET_LINK
            txtForget.setOnClickListener {
                findNavController()
                    .navigate(SignInFragmentDirections.actionSignInToForgetPass())
            }

            chkbxStay.text = IS_STAY_LOG_IN

            btnSignIn.setOnClickListener {
                val email = dtxtName.text.toString()
                val pass = dtxtPass.text.toString()
                val stay = chkbxStay.isChecked

                if (email.isNotBlank() and pass.isNotBlank()) {
                    if (viewModel.signIn(email, pass, stay)) {
                        findNavController()
                            .navigate(SignInFragmentDirections.actionSignInFragmentToHome())
                    } else {
                        Toast.makeText(requireContext(), "ایمیل یا رمز عبور اشتباه است!!", Toast.LENGTH_SHORT).show()
//                        dtxtName.text = ""
//                        dtxtPass.text = ""
//                        dtxtName.isErrorEnabled = true
//                        dtxtPass.isErrorEnabled = true
                    }
                }
            }

            txtAppName.text = APP_NAME
        }
    }

}