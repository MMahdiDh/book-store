package com.example.book.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.book.R
import com.example.book.databinding.ActivityMainBinding
import com.google.android.material.navigation.NavigationBarView

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        navController = this.findNavController(R.id.frgmntHost)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if(destination.id in listOf(R.id.start, R.id.signIn, R.id.signUp, R.id.forgetPass)) {
                binding.btmnvgtn.visibility = NavigationBarView.GONE

            } else {
                binding.btmnvgtn.visibility = NavigationBarView.VISIBLE
            }

        }
        binding.btmnvgtn.setupWithNavController(navController)
    }
}