package com.example.book.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.book.databinding.ItemRecyclerViewBinding
import com.example.book.util.datatype.Category

class RecyclerAdapter(private val activity: FragmentActivity?, val func: (String,String,Int)->Unit)
    : ListAdapter<Category, RecyclerAdapter.VH>(object : DiffUtil.ItemCallback<Category>() {
    override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean =
        newItem.name == oldItem.name

    override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean =
        newItem == oldItem

}) {
    class VH(private val binding: ItemRecyclerViewBinding, private val activity: FragmentActivity?):
        RecyclerView.ViewHolder(binding.root) {
        fun set(dataset:Category, func: (String,String,Int)->Unit) {
            binding.apply {
                txtTitle.text = dataset.name

                rclrCategories.apply {
                    layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
                    adapter = BooksAdapterRecycler(){ bookId ->
                        func("B", "", bookId)
                    }.also { adapter ->
                        if (dataset.books.lastIndex > 0) {
                            adapter.submitList(dataset.books.subList(
                                0,
                                if (dataset.books.lastIndex < 9) dataset.books.lastIndex else 9
                            ))
                        }
                    }
                }

                imgMore.setOnClickListener { func("C", dataset.name, 0) }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH(
        ItemRecyclerViewBinding.inflate(LayoutInflater.from(parent.context), parent,false),
        activity
    )

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.set(getItem(position), func)
    }
}