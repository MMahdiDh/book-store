package com.example.book.ui.screen

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.book.databinding.FragmentNotFoundBinding
import com.example.book.ui.viewmodel.NotFoundViewModel

class NotFoundFragment : Fragment() {

    private lateinit var binding: FragmentNotFoundBinding
    private lateinit var viewModel: NotFoundViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNotFoundBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("RestrictedApi")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[NotFoundViewModel::class.java]

        viewModel.cleanCart()

        binding.apply {
            btnBack.text = "بازگشت"
            btnBack.setOnClickListener {
                findNavController().popBackStack()
            }
        }
    }

}