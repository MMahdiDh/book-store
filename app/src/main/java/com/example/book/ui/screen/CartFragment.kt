package com.example.book.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.book.databinding.FragmentCartBinding
import com.example.book.ui.adapter.CartAdapter
import com.example.book.ui.viewmodel.CartViewModel
import com.example.book.util.COST_SIGN

class CartFragment : Fragment() {

    private lateinit var binding: FragmentCartBinding
    private lateinit var viewModel: CartViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[CartViewModel::class.java]
        viewModel.cartIsEmpty()
        viewModel.pay.observe(viewLifecycleOwner){ paiment ->
            if ((paiment != 0) and (viewModel.send.value!! == 0)) {
                viewModel.post()
            }
        }

        binding.apply {
            viewModel.pay.observe(viewLifecycleOwner){ pay ->
                btnBuy.text = "پرداخت:  ${pay}${COST_SIGN}"
            }
            btnBuy.setOnClickListener {
                findNavController().navigate(
                    CartFragmentDirections.actionCartToNotFoundFragment()
                )
            }
            viewModel.send.observe(viewLifecycleOwner) { txtSend.text = "هزینه ارسال: ${it}${COST_SIGN}" }
            viewModel.off.observe(viewLifecycleOwner) { txtOff.text = " تخفیف: ${it}${COST_SIGN}" }
            viewModel.cost.observe(viewLifecycleOwner) { txtTotal.text = "جمع: ${it}${COST_SIGN}" }

            chkbxNewAddress.text = "اضافه کردن آدرس جدید:"
            chkbxNewAddress.setOnClickListener {
                if (chkbxNewAddress.isChecked) {
                    crdAddress.visibility = View.VISIBLE
                } else {
                    crdAddress.visibility = View.GONE
                }
            }
            rclCart.apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = CartAdapter(){ count, price, off ->
                    viewModel.update(count, price, off)
                }.also { adapter ->
                    viewModel.cart.observe(viewLifecycleOwner) { dataset ->
                        adapter.submitList(dataset)
                    }
                }
            }.also { viewModel.getBooks() }

            txtEmpty.text = "سبد خرید شما خالی است!"
//            if (viewModel.isEmpty()) {
//                cnstltEmpty.visibility = View.VISIBLE
//                lnrltNotEmpty.visibility = View.GONE
//            } else {
//                cnstltEmpty.visibility = View.GONE
//                lnrltNotEmpty.visibility = View.VISIBLE
//            }
        }
    }

}