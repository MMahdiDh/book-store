package com.example.book.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.book.repository.Repository
import com.example.book.repository.Repository.getBooksList
import com.example.book.util.datatype.Book
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private var _books = MutableLiveData<List<Book>>()
    val books: LiveData<List<Book>>
        get() = _books

    fun getBooksList() {
        viewModelScope.launch {
            _books.value = Repository.getBooksList()
        }
    }
}